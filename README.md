# Cheatsheet

[[_TOC_]]

## docker
```bash
docker run -it  --mount src="$(pwd)",target=/path_in_docker_container,type=bind  <DockerImage>
```

```bash
alias dockerrunmount='docker run --mount src="`pwd`",target=/data,type=bind'
```

## ssh
### „Warning: Remote host identification has changed“
```bash
ssh-keygen -R 192.168.1.123
```

## Bash
```bash
array=(
    "this/is/a/test.path/to/check/correct.result./.in..bash.tar.gz"
    "this/is/a/test.path/to/check/correct.result./.in..bash.tar.gz/"
    "this/is/a/test.path/to/check/correct.result./.in..bash.tar.gz/qwe"
)
for filename in "${array[@]}"
do
    file="${filename##*/}"
    basename="${file%.*}"
    extension=$([[ "$file" = *.* ]] && echo ".${file##*.}" || echo '')
    
    echo
    echo "filename: $filename"
    echo "file: $file"
    echo "basename: $basename"
    echo "extension: $extension"
done
```

```bash
filename: this/is/a/test.path/to/check/correct.result./.in..bash.tar.gz
file: .in..bash.tar.gz
basename: .in..bash.tar
extension: .gz

filename: this/is/a/test.path/to/check/correct.result./.in..bash.tar.gz/
file: 
basename: 
extension: 

filename: this/is/a/test.path/to/check/correct.result./.in..bash.tar.gz/qwe
file: qwe
basename: qwe
extension: 
```

## MacOS - lock many files at once
* Select files
* <kbd>⌥ Option</kbd> + <kbd>⌘ Command</kbd> + <kbd>I</kbd>


## MacOS - Quick Look for text content

```bash
brew install --cask qlcolorcode
```

On macOS Ventura, remove the quarantine attribute from the plugins:

```bash
# View quarantine attributes:
xattr -r ~/Library/QuickLook

# Remove quarantine attributes:
xattr -d -r com.apple.quarantine ~/Library/QuickLook
```

Restart Finder:  > Force Quit Applications > Select Finder > Relaunch

Add Language Types as described here: https://github.com/anthonygelibert/QLColorCode


## MacOS - Create app to run shell script
* Create new Automator program
* Add applescript block
```applescript
on run {input, parameters}
	
	set args to ""
	repeat with f in input
		set args to args & " " & quoted form of POSIX path of f
	end repeat
	
	tell application "Terminal"
		activate
		do script "bash /path/to/my/script.sh" & args
	end tell
	
	return input
end run

# Source: https://apple.stackexchange.com/a/114420
```

or

```applescript
on run {input, parameters}
	set args to ""
	repeat with f in input
		set args to args & " " & quoted form of POSIX path of f
	end repeat
	try
		set thePath to POSIX path of (path to resource "worker.sh")
		tell application "Terminal"
			activate
			do script "bash " & thePath & args
		end tell
		on error errmess
 		    display dialog errmess
 	end try
	return input
end run
```
and copy worker.sh to the Apps Contents/Resources folder

## MacOS - Custom margins / border widths in TextEdit
* Save Document as a Rich Text file (.rtf)
* Close Document
* Open Document in RAW Texteditor
* Change values *margl&lt;Twips&gt;* (margin left) and *margl&lt;Twips&gt;* (margin right)
* Optionally add *margb&lt;Twips&gt;* (margin bottom) and *margt&lt;Twips&gt;* (margin top)
* Save file
* Open File in TextEdit

Example line:
```
\paperw11900\paperh16840\margl1414\margr567\margb958\margt958\vieww17440\viewh15820\viewkind1
```
Source: https://hints.macworld.com/article.php?story=20140330121905819


## Raspberry Pi OS headless setup (Buster and Bullseye)
```bash
# Enter boot directory on SD card
cd /Volumes/boot

# Enable ssh
touch ./ssh

# Setup WIFI
echo """country=US
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid=\"NETWORK-NAME\"
    psk=\"NETWORK-PASSWORD\"
}
""" > ./wpa_supplicant.conf


# Add User pi:raspberry
echo "pi:$6$/4.VdYgDm7RJ0qM1$FwXCeQgDKkqrOU3RIRuDSKpauAbBvP11msq9X58c8Que2l1Dwq3vdJMgiZlQSbEXGaY5esVHGBNbCxKLVNqZW1" >> userconf
```